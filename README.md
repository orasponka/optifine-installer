# optifine-installer
optifine-installer is cli tool that can install OptiFine mod for many Minecraft versions. <br>
optifine-installer is now available officially only for Arch Linux and Arch Linux based distros.

**I don't own OptiFine or files in versions folder and versions.zip!** <br>
**My optifine-installer script is licensed under MIT License but OptiFine it's self is not!** <br> <br>
Find and download Optifine [here.](https://optifine.net)

### Installation
#### Arch Linux:
If you have enabled my [op-arch-repo,](https://gitlab.com/orasponka/op-arch-repo) you can install optifine-installer via pacman.
```
sudo pacman -S optifine-installer
```
If not, do things below.
```
git clone https://gitlab.com/orasponka/optifine-installer.git
cd optifine-installer
makepkg
sudo pacman -U *.zst
```

### Usage
Find all flags and options on optifine-installer's man page.
```
man optifine-installer
```

### Requirments
**Scripts need these commands to run properly:** bash, find, java, man <br>
**Arch Linux build package needs**: git, makepkg, sudo
