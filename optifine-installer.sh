#!/usr/bin/env bash

versionsDir="/usr/share/optifine-installer/versions"
versionsList=("1.7.2" "1.7.10" "1.8.0" "1.8.8" "1.8.9" "1.9.0" "1.9.2" "1.9.4" "1.10" "1.10.2" "1.11" "1.11.2" "1.12" "1.12.1" "1.12.2" "1.13" "1.13.1" "1.13.2" "1.14.2" "1.14.3" "1.14.4"
              "1.15.2" "1.16.1" "1.16.2" "1.16.3" "1.16.4" "1.16.5" "1.17" "1.17.1" "1.18" "1.18.1" "1.18.2" "1.19" "1.19.1" "1.19.2")

if [ -f "/bin/java" ]
then

    if [[ $1 = "-l" || $1 = "--list-versions" || $1 = "l" ]]
    then
        for version in ${versionsList[@]}
        do
        echo $version
        done
    fi

    if [[ $1 = "-lf" || $1 = "--list-versions-fullpaths" || $1 = "lf" ]]
    then
        find $versionsDir -type f -name "*"
    fi

    if [[ $1 = "-v" || $1 = "-version" || $1 = "v" ]]
    then
        echo "Version: 4.4"
    fi

    if [[ $1 = "-h" || $1 = "-help" || $1 = "h" ]]
    then
        man optifine-installer
    fi

    if [[ $1 = "-i" || $1 = "-install" || $1 = "i" ]]
    then
        if [ -f "$versionsDir/OptiFine_$2.jar" ]
        then
            java -jar "$versionsDir/OptiFine_$2.jar"
        else
            echo "$versionsDir/OptiFine_$2.jar not found"
        fi
    fi

    if [[ $1 = "-ic" || $1 = "--install-using-current-directory" || $1 = "ic" ]]
    then
        if [ -f "./OptiFine_$2.jar" ]
        then
            java -jar "./OptiFine_$2.jar"
        else
            echo "./OptiFine_$2.jar not found"
        fi
    fi

else
    echo "/bin/java not found"
fi
